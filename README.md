ics-ans-debug
=============

Ansible debug playbook. It currently print all variables.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
